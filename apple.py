from termcolor import colored
"""
Grocery assistant.
Note, it may be only 0-30 apples.
If other quantity is required, the answer is "Столько нет"
"""
def main():
    """Add "яблоко" word."""
    n = int(input("Сколько яблок Вам нужно?\n"))
    if 22 <= n <=24 or 2 <= n <=4:
        print("Пожалуйста,", n, "яблока")
    elif n == 1 or n == 21:
        print("Пожалуйста,", n, "яблоко")
    elif 0 <= n <= 30:
        print("Пожалуйста,", n, "яблок")
    else:
        print("Столько нет")

def termcolor(label):
    gross = colored(label, color="red")
    print(gross)

if __name__ == "__main__":
    termcolor("Grocery assistant")
    main()

